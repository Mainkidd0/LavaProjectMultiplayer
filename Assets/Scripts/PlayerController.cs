using MoreMountains.CorgiEngine;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerController : MonoBehaviourPunCallbacks
{

    public PhotonView photonView;
    private bool _isLeaved = false;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<CharacterJump>().IsPhotonMine = false;
        photonView = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!photonView.IsMine) return;
       

        GetComponent<CharacterJump>().IsPhotonMine = photonView.IsMine;
        GetComponent<CharacterHorizontalMovement>().IsPhotonMine = photonView.IsMine;
        GetComponent<CorgiController>().IsPhotonMine = photonView.IsMine;

        if (Input.GetKey(KeyCode.LeftArrow)) 
            transform.Translate(-Time.deltaTime * 5, 0, 0);
        if (Input.GetKey(KeyCode.RightArrow))
            transform.Translate(Time.deltaTime * 5, 0, 0);
    }



}
