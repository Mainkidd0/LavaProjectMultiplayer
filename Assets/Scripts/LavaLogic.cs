using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaLogic : MonoBehaviour
{
    public GameManagerMain gameManagerMain;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag=="Player")
        {
            Debug.Log("Collision with lava started.");
            gameManagerMain.ShowLoseScreen();
        }
    }
}
