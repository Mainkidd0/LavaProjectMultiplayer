using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class WaitRoomLogic : MonoBehaviourPunCallbacks, IOnEventCallback, MMEventListener<CorgiEngineEvent>
{
    public Text LogText;
    public Button PlayButton;
    public List<GameObject> avatars;
    public GameObject PlayerPrefab;
    // Start is called before the first frame update
    void Start()
    {
        //GameObject go = PhotonNetwork.Instantiate(PlayerPrefab.name, Vector3.zero, Quaternion.identity);
        //StartCoroutine(LeaveCoroutine());
    }

    IEnumerator LeaveCoroutine()
    {
        Debug.Log("LEAVING COROUTINE");
        yield return new WaitForSeconds(1.5f);

        PhotonNetwork.LeaveRoom(true);
        //SceneManager.LoadScene("Lobby");
    }

    // Update is called once per frame
    void Update()
    {
        if (PhotonNetwork.CurrentRoom != null)
        {
           LogText.text="Players: " +PhotonNetwork.CurrentRoom.PlayerCount;
        }
        for (int i=0; i< PhotonNetwork.CurrentRoom.PlayerCount; i++)
        {
            avatars[i].SetActive(true);
        }

        if (!PhotonNetwork.IsMasterClient)
        {
            PlayButton.enabled = false;
            PlayButton.GetComponent<RectTransform>().localScale = Vector3.zero;
        }
        
    }

    public override void OnLeftRoom()
    {
        Debug.Log("LEFT ROOM");
        base.OnLeftRoom();
    }

    public void LoadScene()
    {

        PhotonNetwork.LoadLevel("Lava");
        //int a = 1;
        //RaiseEventOptions op = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
        //SendOptions sendOptions = new SendOptions { Reliability = true };
        //PhotonNetwork.RaiseEvent(42, a, op, sendOptions);
        //StartGame();
        //PlayButton.enabled = false;
    }

    public void StartGame()
    {
        GameObject go = PhotonNetwork.Instantiate(PlayerPrefab.name, Vector3.zero, Quaternion.identity);
        StartCoroutine(LeaveCoroutine());
    }


    public void OnEvent(EventData photonEvent)
    {
        switch (photonEvent.Code)
        {
            case 42:
                Debug.Log("EVENT CALLED!");
                break;
        }
    }

    public void OnMMEvent(CorgiEngineEvent engineEvent)
    {
        switch (engineEvent.EventType)
        {
            case CorgiEngineEventTypes.PlayerDeath:
                StartCoroutine(LeaveCoroutine());
                break;
        }
    }


    //protected virtual void OnEnable()
    //{
    //    this.MMEventStartListening<CorgiEngineEvent>();
    //}

    ///// <summary>
    ///// OnDisable, we stop listening to events.
    ///// </summary>
    //protected virtual void OnDisable()
    //{
    //    this.MMEventStopListening<CorgiEngineEvent>();
    //}
}
