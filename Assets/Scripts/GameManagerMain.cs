using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using UnityEngine.SceneManagement;

public class GameManagerMain : MonoBehaviourPunCallbacks, MMEventListener<CorgiEngineEvent>
{
    public GameObject PlayerPrefab;
    public GameObject CameraUI;
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.LogLevel = PunLogLevel.Full;
        PhotonNetwork.SendRate = 60;
        //PhotonNetwork.SerializationRate = 60;
        StartCoroutine(ExampleCoroutine());
    }
    
    

    IEnumerator ExampleCoroutine()
    {
        yield return new WaitForSeconds(1);
        GameObject go = PhotonNetwork.Instantiate(PlayerPrefab.name, Vector3.zero, Quaternion.identity);
        LevelManager.Instance.Players.Add(go.GetComponent<Character>());
        MMEventManager.TriggerEvent(new CorgiEngineEvent(CorgiEngineEventTypes.LevelStart));
        go.GetComponent<Character>().PlayerID = PhotonNetwork.NickName;
        CameraUI.GetComponent<InputManager>().PlayerID= PhotonNetwork.NickName;
        //Debug.Log("SPAWNED!");

        //yield return new WaitForSeconds(Random.Range(2.0f,15.0f));
        //StartCoroutine(LeaveCoroutine());
    }

    IEnumerator LeaveCoroutine()
    {
        Debug.Log("LEAVING COROUTINE");
        
        yield return new WaitForSeconds(1.5f);
        
        PhotonNetwork.LeaveRoom(true);
        //SceneManager.LoadScene("Lobby");
    }

    public void ShowLoseScreen()
    {
        StartCoroutine(LeaveCoroutine());
    }

    public virtual void OnMMEvent(CorgiEngineEvent engineEvent)
    {
        switch (engineEvent.EventType)
        {
           case CorgiEngineEventTypes.PlayerDeath:

             StartCoroutine(LeaveCoroutine());
               
               break;
        }
    }


    public override void OnEnable()
    {
        base.OnEnable();
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    /// <summary>
    /// OnDisable, we stop listening to events.
    /// </summary>
    public virtual void OnDisable()
    {
        base.OnDisable();
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(PhotonNetwork.GetPing());
    }

    public override void OnLeftRoom()
    {
        Debug.Log("ROOM LEFT");
        PhotonNetwork.LoadLevel("Lobby");
        base.OnLeftRoom();
    }

    public void Leave()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(newPlayer.NickName + " Entered the room!");
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.Log(otherPlayer.NickName + " Left the room!");
    }

    
}
